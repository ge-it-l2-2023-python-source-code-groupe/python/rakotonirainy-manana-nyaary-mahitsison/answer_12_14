import random
position = 0
final_position = 5
jumps = 0

while position != final_position:
  
    jump = random.choice([1, -1])
    position += jump
    jumps += 1

print(f"La puce est arrivée à l'emplacement final en {jumps} sauts.")