def fib_seq(n):
    fib_values = [0, 1]
    while len(fib_values) < n:
        fib_values.append(fib_values[-1] + fib_values[-2])
    return fib_values

num_fib = 10 # le nombre de valeurs de fibonnaci que vous souhaitez afficher

for i in range(num_fib):
    print(fib_seq(i)[-1])